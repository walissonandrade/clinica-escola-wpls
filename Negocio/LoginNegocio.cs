﻿using Model;
using Repositorio;

namespace Negocio
{
    public class LoginNegocio : BaseNegocio<LoginEntidade>
    {
        public LoginNegocio() : base(new LoginRepositorio()) { }
    }
}