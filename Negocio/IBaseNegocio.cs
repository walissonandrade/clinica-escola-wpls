﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Negocio
{
    public interface IBaseNegocio<T> where T : EntidadeBase
    {
        T Inserir(T aEntidade);
        void Deletar(T aEntidade);
        IEnumerable<T> Buscar();
        void Atualizar(T aEntidade);
        T BuscarPorIdentificador(long aIdentificador);
        void Salvar();
        //IQueryable<T> BuscarPorFiltro(Expression<Func<T, bool>> aFiltro);
        IQueryable<T> BuscarPorFiltro(Expression<Func<T, bool>> aFiltro, params string[] aPropriedade);
    }
}