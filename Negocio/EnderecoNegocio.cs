﻿using Model;
using Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class EnderecoNegocio : BaseNegocio<EnderecoEntidade>
    {
        public EnderecoNegocio() : base(new EnderecoRepositorio()) { }
    }
}