﻿using Model;
using Repositorio;
using System.Linq;
using Utilitario;

namespace Negocio
{
    public class PacienteNegocio : BaseNegocio<PacienteEntidade>
    {
        public PacienteNegocio() : base(new PacienteRepositorio()) {}

        public PacienteEntidade BuscarPacientePorNomeOuCPF(string aValor, string aCpfOuNome)
        {
            PacienteEntidade pacienteEntidade = null;

            if (!string.IsNullOrWhiteSpace(aCpfOuNome))
            {
                if (aCpfOuNome == "Nome")
                {
                    pacienteEntidade = BuscarPorFiltro(a => a.Nome.Contains(aValor)).FirstOrDefault();
                }
                else if (aCpfOuNome == "CPF")
                {
                    aValor = aValor.RemoverMascara();
                    pacienteEntidade = BuscarPorFiltro(a => a.CPF.Contains(aValor)).FirstOrDefault();
                }
            }

            return pacienteEntidade;
        }
    }
}