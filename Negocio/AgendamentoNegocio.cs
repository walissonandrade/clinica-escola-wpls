﻿using Enumerador;
using Model;
using Repositorio;
using System;

namespace Negocio
{
    public class AgendamentoNegocio : BaseNegocio<AgendamentoEntidade>
    {
        public AgendamentoNegocio() : base(new AgendamentoRepositorio()) { }

        public AgendamentoEntidade CriarAgendamentoPaciente(long aPessoaID, DateTime aDataAgendamento)
        {
            AgendamentoEntidade agendamento = new AgendamentoEntidade();

            agendamento.DataAgendamento = aDataAgendamento;
            agendamento.PacienteID = aPessoaID;
            agendamento.SituacaoAgendamento = SituacaoAgendamentoEnum.NaoAtendido;

            Inserir(agendamento);
            Salvar();

            return agendamento;
        }
    }
}