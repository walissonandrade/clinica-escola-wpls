﻿using Model;
using Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Negocio
{
    public abstract class BaseNegocio<T> : IBaseNegocio<T> where T : EntidadeBase
    {
        IBaseRepositorio<T> _Repositorio;

        protected IBaseRepositorio<T> Dados { get { return _Repositorio; } private set { _Repositorio = value; } }

        public BaseNegocio(IBaseRepositorio<T> aRepositorio)
        {
            _Repositorio = aRepositorio;
        }

        public virtual T Inserir(T aEntidade)
        {
            if (aEntidade == null)
                throw new ArgumentNullException("Model");

            aEntidade = _Repositorio.Inserir(aEntidade);

            return aEntidade;
        }

        public virtual void Atualizar(T aEntidade)
        {
            if (aEntidade == null)
                throw new ArgumentNullException("Model");

            _Repositorio.Atualizar(aEntidade);
        }

        public virtual void Deletar(T aEntidade)
        {
            if (aEntidade == null)
                throw new ArgumentNullException("Model");

            _Repositorio.Deletar(aEntidade);
        }

        public virtual IEnumerable<T> Buscar()
        {
            return _Repositorio.Buscar();
        }

        public T BuscarPorIdentificador(long aIdentificador)
        {
            return _Repositorio.BuscarPorIdentificador(aIdentificador);
        }

        public void Salvar()
        {
            _Repositorio.Salvar();
        }

        public virtual IQueryable<T> BuscarPorFiltro(Expression<Func<T, bool>> aFiltro, params string[] aPropriedade)
        {
            return Dados.BuscarPorFiltro(aFiltro, aPropriedade);
        }
    }
}