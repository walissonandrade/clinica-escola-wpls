﻿using Model;

namespace Repositorio
{
    public class PacienteRepositorio : BaseRepositorio<PacienteEntidade>
    {
        public PacienteRepositorio() : base(new ClinicaContexto()) { }
    }
}