﻿using Model;

namespace Repositorio
{
    public class LoginRepositorio : BaseRepositorio<LoginEntidade>
    {
        public LoginRepositorio() : base(new ClinicaContexto()) { }
    }
}