﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio
{
    public class EnderecoRepositorio : BaseRepositorio<EnderecoEntidade>
    {
        public EnderecoRepositorio() : base(new ClinicaContexto()) { }
    }
}