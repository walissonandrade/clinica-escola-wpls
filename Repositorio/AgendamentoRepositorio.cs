﻿using Model;

namespace Repositorio
{
    public class AgendamentoRepositorio : BaseRepositorio<AgendamentoEntidade>
    {
        public AgendamentoRepositorio() : base(new ClinicaContexto()) { }
    }
}