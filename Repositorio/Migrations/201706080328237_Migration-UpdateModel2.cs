namespace Repositorio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrationUpdateModel2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pessoa", "DataFilaEspera", c => c.DateTime());
            DropColumn("dbo.Pessoa", "DataCadastro");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Pessoa", "DataCadastro", c => c.DateTime());
            DropColumn("dbo.Pessoa", "DataFilaEspera");
        }
    }
}
