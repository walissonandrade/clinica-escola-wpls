namespace Repositorio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CriacaoBD : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agendamento",
                c => new
                    {
                        AgendamentoID = c.Long(nullable: false, identity: true),
                        DataAgendamento = c.DateTime(nullable: false),
                        SituacaoAgendamento = c.Int(nullable: false),
                        PacienteID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.AgendamentoID)
                .ForeignKey("dbo.Pessoa", t => t.PacienteID, cascadeDelete: true)
                .Index(t => t.PacienteID);
            
            CreateTable(
                "dbo.Pessoa",
                c => new
                    {
                        PessoaID = c.Long(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 90),
                        TelefoneFixo = c.String(maxLength: 14),
                        TelefoneCelular = c.String(maxLength: 14),
                        Email = c.String(maxLength: 90),
                        CPF = c.String(),
                        RG = c.String(),
                        DataNascimento = c.DateTime(nullable: false),
                        Sexo = c.Int(nullable: false),
                        EstadoCivil = c.Int(nullable: false),
                        QuantidadeFilhos = c.Byte(nullable: false),
                        Escolaridade = c.Int(nullable: false),
                        Profissao = c.String(),
                        DescricaoConhecimentoClinica = c.String(),
                        InstituicaoEncaminhamento = c.String(),
                        CursoFAPBetim = c.String(),
                        Logradouro = c.String(nullable: false, maxLength: 90),
                        Numero = c.Int(),
                        Cep = c.String(),
                        Bairro = c.String(maxLength: 80),
                        Cidade = c.String(maxLength: 80),
                        Estado = c.String(nullable: false, maxLength: 40),
                        Pais = c.String(nullable: false, maxLength: 40),
                        DataFilaEspera = c.DateTime(),
                        FilaEspera = c.Boolean(),
                    })
                .PrimaryKey(t => t.PessoaID);
            
            CreateTable(
                "dbo.Login",
                c => new
                    {
                        LoginID = c.Long(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 30),
                        Senha = c.String(nullable: false, maxLength: 30),
                        RespostaSecreta = c.String(nullable: false, maxLength: 70),
                        Email = c.String(maxLength: 80),
                    })
                .PrimaryKey(t => t.LoginID)
                .Index(t => t.Login, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Agendamento", "PacienteID", "dbo.Pessoa");
            DropIndex("dbo.Login", new[] { "Login" });
            DropIndex("dbo.Agendamento", new[] { "PacienteID" });
            DropTable("dbo.Login");
            DropTable("dbo.Pessoa");
            DropTable("dbo.Agendamento");
        }
    }
}
