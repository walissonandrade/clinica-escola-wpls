﻿using Model;
using System.Configuration;
using System.Data.Entity;
using System.Diagnostics;

namespace Repositorio
{
    /// <summary>
    /// Classe que faz a interligação do banco de dados com a aplicação
    /// </summary>
    public class ClinicaContexto : DbContext
    {
        /// <summary>
        /// Construtor default, pega a connection string no Web.config e decripta
        /// </summary>
        public ClinicaContexto() : base(ConfigurationManager.AppSettings["connection"])
        {
        }

        public DbSet<PessoaEntidade> Pessoas { get; set; }
        public DbSet<PacienteEntidade> Pacientes { get; set; }
        public DbSet<LoginEntidade> Logins { get; set; }
        public DbSet<AgendamentoEntidade> Agendamentos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
#if DEBUG

            Database.Log = a => Debug.Write(a);

#endif
        }
    }
}