﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Repositorio
{
    public interface IBaseRepositorio<T> : IDisposable where T : EntidadeBase
    {
        IEnumerable<T> Buscar();
        //IQueryable<T> BuscarPorFiltro(Expression<Func<T, bool>> aFiltro);
        IQueryable<T> BuscarPorFiltro(Expression<Func<T, bool>> aFiltro, params string[] aPropriedade);
        T BuscarPorIdentificador(long aIdentificador);
        T Inserir(T aEntidade);
        void Deletar(T aEntidade);
        void Atualizar(T aEntidade);
        void Salvar();
    }
}