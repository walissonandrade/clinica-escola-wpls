﻿var indiceAtual = -1;

function adicionarEventos() {
    document.addEventListener('keypress', function () {
        formatarMascara(document.getElementById('inpCpf'), '###.###.###-##');
    });

    document.addEventListener('keypress', function () {
        formatarMascara(document.getElementById('inpCep'), '##.###-###');
    });

    mascaraTelefoneFixo('inpTelefoneFixo');
    mascaraTelefoneCelular('inpTelefoneCelular');

    $('#inpCep').on('change', function () {
        buscarCep($(this).val());
    });

    $('#btnBuscarNomeouCpf').on('click', function () {
        $.ajax({
            url: 'BuscarCliente',
            data: {
                aValor: $('#inpBuscarNomeouCpf').val(),
                aCpfOuNome: $('input[name=optionsRadiosInlineBusca]:checked').val()
            },
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                if (data != null && data != "Paciente não encontrado.") {
                    exibirInserirEditar();

                    $('#inpCodigo').val(data.PessoaID);
                    $('#inpCpf').val(data.CPF);
                    $('#inpNome').val(data.Nome);
                    $('#inpCep').val(data.Cep);
                    $('#inpEndereco').val(data.Logradouro);
                    $('#inpBairro').val(data.Bairro);
                    $('#selectUf').val(data.Estado);
                    $('#inpTelefoneFixo').val(data.TelefoneFixo);
                    $('#inpTelefoneCelular').val(data.TelefoneCelular);
                    $('#inpEmail').val(data.Email);

                    var dataNascimento = new Date(parseInt(data.DataNascimento.replace("/Date(", "").replace(")/", "")));

                    var day = ("0" + dataNascimento.getDate()).slice(-2);
                    var month = ("0" + (dataNascimento.getMonth() + 1)).slice(-2);

                    var today = dataNascimento.getFullYear() + "-" + (month) + "-" + (day);

                    $('#inpDataNascimento').val(today);

                    if (data.Sexo == 1) {
                        $('#ddlSexo').val('Masculino');
                    }
                    else if (data.Sexo == 2) {
                        $('#ddlSexo').val('Feminino');
                    }
                    else if (data.Sexo == 3) {
                        $('#ddlSexo').val('NaoDeclarado');
                    }
                }
                else {
                    exibirModal('Alerta', data);
                }
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        });
    });

    //$('#inpBuscarNomeouCpf').autocomplete({
    //    source: function (request, response) {
    //        $.ajax({
    //            url: 'CadastroCliente/AutoCompleteBuscaCliente',
    //            data: {
    //                aValor: $('inpBuscarNomeouCpf').val(),
    //                aCpfOuNome: $('input[name=optionsRadiosInlineBusca]').val()
    //            },
    //            success: function (data) {
    //                response($.map(data.d, function (item) {
    //                    return { value: item }
    //                }))
    //            }
    //        });
    //    }
    //});

    $('input[name=optionsRadiosInlineBusca]').on('change', function () {
        $('#inpBuscarNomeouCpf').val('');

        if ($('input[name=optionsRadiosInlineBusca]').val() != null) {
            $('#inpBuscarNomeouCpf').removeAttr('disabled');

            if ($(this).val() == 'Nome') {
                $('#inpBuscarNomeouCpf').attr('placeholder', 'Informe o nome').attr('maxlength', '90').off('keypress');
            }
            else {
                $('#inpBuscarNomeouCpf').attr('placeholder', 'Informe o CPF').attr('maxlength', '14').on('keypress', function () {
                    formatarMascara(document.getElementById('inpBuscarNomeouCpf'), '###.###.###-##');
                });
            }
        }
        else {
            $('#inpBuscarNomeouCpf').attr('disabled', '');
        }
    });
}

function adicionarEventosViewFila() {
    $('#btnBuscarClienteFila').on('click', function () {
        $.ajax({
            url: '../CadastroCliente/BuscarCliente',
            data: {
                aValor: $('#buscarPorNomeOuCpf').val(),
                aCpfOuNome: $('input[name=buscarNomeCPF]:checked').val()
            },
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                if (data != null && data != "Paciente não encontrado.") {
                    $('#dadosPaciente').empty();
                    $('#dadosPaciente').append('<b><p>Nome: <span id="dadosNome">' + data.Nome + '</span><br />' + 'CPF: <span id="dadosCPF">' + data.CPF + '</span>' + '</p></b>');
                    $('#btnInserirClienteFila').removeAttr('disabled');
                }
                else {
                    $('#btnInserirClienteFila').attr('disabled', '');
                    exibirModal('Alerta', data);
                }
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        });
    });

    $('#btnInserirClienteFila').on('click', function () {
        $.ajax({
            url: 'InserirClienteFila',
            data: {
                aValor: $('#buscarPorNomeOuCpf').val(),
                aCpfOuNome: $('input[name=buscarNomeCPF]:checked').val()
            },
            dataType: 'text',
            type: 'GET',
            success: function (data) {
                if (data != null && data != "Paciente não encontrado.") {
                    var ultimaLinha = $('#tblFilaEspera').find('tbody').find('tr:last').attr('id');
                    var numeroLinha = 0;

                    if (ultimaLinha != numeroLinha && ultimaLinha != undefined) {
                        numeroLinha = parseInt(ultimaLinha.replace('linha', '')) + 1;
                    }

                    var linha = '';
                    var nome = $('#dadosNome').text();
                    var cpf = $('#dadosCPF').text();

                    if ($('#tblFilaEspera').find('tbody').find('tr:contains(' + cpf + ')').length == 0) {
                        linha = '<tr id="linha' + numeroLinha + '"><td>' + cpf + '</td><td>' + nome + '</td><td>' + formatarDataCSharp(data) + '<td class="actions"><a class="btn btn-info btn-xs" data-toggle="modal" data-target="#ModalAgendar" onclick="indiceAtual = ' + numeroLinha + '">Agendar</a><a style="margin-left: 6px !important;" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-modal" onclick="removerFila(' + numeroLinha + ')">Remover da fila</a></td></tr>';
                        $('#tblFilaEspera').find('tbody').append(linha);
                        exibirModal('Alerta', 'Paciente inserido na fila de espera com sucesso.');
                    }
                    else {
                        exibirModal('Alerta', 'Paciente já está cadastrado na fila de espera');
                    }
                }
                else {
                    exibirModal('Alerta', data);
                }
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        });
    });
}

function adicionarEventosViewAgendamento() {
    buscarListaAgendamentos();

    $('#btnBuscarAgendamento').on('click', function () {
        $.ajax({
            url: '../CadastroCliente/BuscarCliente',
            data: {
                aValor: $('#buscarPorNomeOuCpf').val(),
                aCpfOuNome: $('input[name=rbtBuscaCpfNome]:checked').val()
            },
            dataType: 'json',
            type: 'GET',
            success: function (data) {
                if (data != null && data != "Paciente não encontrado.") {
                    $('#txtDataAgendamento').val('');
                    $('#dadosAgendamento').css('display', 'block');
                    $('#pacienteAgendamento').append('Nome: <span id="dadosNome">' + data.Nome + '</span><br />' + 'CPF: <span id="dadosCPF">' + data.CPF + '</span>');
                    $('#btnAgendarConsulta').removeAttr('disabled');
                }
                else {
                    $('#btnAgendarConsulta').attr('disabled', '');
                    exibirModal('Alerta', data);
                }
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        });
    });

    $('#btnAgendarConsulta').on('click', function () {
        $.ajax({
            url: 'CriarAgendamentoPaciente',
            data: {
                aValor: $('#buscarPorNomeOuCpf').val(),
                aCpfOuNome: $('input[name=rbtBuscaCpfNome]:checked').val(),
                aDataAgendamento: formatarDataHoraJS($('#txtDataAgendamento').val())
            },
            dataType: 'json',
            type: 'GET',
            beforeSend: function () {
                $('.loading-gif').html('<img alt="loading" src="/Images/loading.gif" />');
            },
            success: function (data) {
                $('.loading-gif').html('');

                if (data != null && data != "Paciente não encontrado.") {
                    var ultimaLinha = $('#tblAgendamento').find('tbody').find('tr:last').attr('id');
                    var numeroLinha = 0;

                    if (ultimaLinha != numeroLinha && ultimaLinha != undefined) {
                        numeroLinha = parseInt(ultimaLinha.replace('linha', '')) + 1;
                    }

                    var linha = '';
                    var nome = $('#dadosNome').text();

                    linha = '<tr id="linha' + numeroLinha + '"><td>' + data.AgendamentoID + '</td><td>' + nome + '</td><td>' + formatarDataHoraJSON(data.DataAgendamento)
                            + '<td class="actions"><a class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-modal" onclick="removerAgendamento(' + numeroLinha + ')">Excluir agendamento</a></td></tr>';
                    $('#tblAgendamento').find('tbody').append(linha);
                    exibirModal('Informação', 'Agendamento cadastrado com sucesso. Foi enviado um email para o paciente informando a data da consulta.');
                }
                else {
                    exibirModal('Alerta', data);
                }
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        });
    });
}

function buscarListaAgendamentos() {
    $.ajax({
        url: 'BuscarListaAgendamentos',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            var linha = '';

            $.each(data, function (index, value) {
                var contador = 0;

                linha = '<tr id="linha' + contador + '"><td>' + value.Codigo + '</td><td>' + value.Nome + '</td><td>' + formatarDataHoraJSON(value.DataAgendamento)
                    + '</td><td class="actions"><a class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-modal" onclick="removerAgendamento(' + contador + ')">Excluir agendamento</a></td></tr>';
                $('#tblAgendamento').find('tbody').append(linha);

                contador++;
            });
        }
    });
}

function agendarConsulta(index) {
    $.ajax({
        url: 'AgendarConsulta',
        type: 'get',
        data: {
            CPF: $('#tblFilaEspera').find('#linha' + index).find('td').eq(0).text(),
            aDataAgendamento: formatarDataHoraJS($('#txtDataAgendamento').val())
        },
        success: function () {
            $('#tblFilaEspera').find('#linha' + index).remove();
            $('#txtDataAgendamento').val('');
            exibirModal('Informação', 'Agendamento cadastrado com sucesso. Foi enviado um email para o paciente informando a data da consulta.');
        }
    });
}

function removerAgendamento(index) {
    $.ajax({
        url: 'RemoverAgendamento',
        type: 'get',
        data: { Codigo: $('#tblAgendamento').find('#linha' + index).find('td').eq(0).text() },
        success: function () {
            $('#tblAgendamento').find('#linha' + index).remove();
            exibirModal('Informação', 'Agendamento desmarcado com sucesso.');
        }
    });
}

function exibirPainelCadastro() {
    $('#btnEditar').on('click', function () {
        limparCamposDiv('divInserirCliente');
        habilitarCampos('divInserirCliente', true);
        exibirEditar();
    });

    $('#btnInserir').on('click', function () {
        limparCamposDiv('divInserirCliente');
        habilitarCampos('divInserirCliente', true);
        exibirInserir();
    });
}

function habilitarCampos(div, habilitar) {
    if (habilitar) {
        $('#' + div + ' *').removeAttr('disabled');
    }
    else {
        $('#' + div + ' *').attr('disabled', '');
    }

    $('#inpCodigo').attr('disabled', '');
}

function limparCamposDiv(div) {
    $('#' + div).find('input').val('');
    //$('#' + div).find('input:radio:checked').removeAttr('checked');
}

function exibirEditar() {
    $('#divBuscaCliente').removeClass('div-invisivel');
    $('#divBuscaCliente').addClass('div-visivel');
    $('#divInserirCliente').removeClass('div-visivel');
    $('#divInserirCliente').addClass('div-invisivel');
}

function exibirInserir() {
    $('#divInserirCliente').removeClass('div-invisivel');
    $('#divInserirCliente').addClass('div-visivel');
    $('#divBuscaCliente').removeClass('div-visivel');
    $('#divBuscaCliente').addClass('div-invisivel');
}

function exibirInserirEditar() {
    $('#divInserirCliente').removeClass('div-invisivel');
    $('#divInserirCliente').addClass('div-visivel');
    $('#divBuscaCliente').removeClass('div-invisivel');
    $('#divBuscaCliente').addClass('div-visivel');
}

function mascaraTelefoneFixo(telefone) {
    $('#' + telefone).on('keypress', function () {
        if ($(this).val().length == 0) {
            $(this).val('(' + $(this).val());
        }

        if ($(this).val().length == 3) {
            $(this).val($(this).val() + ')');
        }

        if ($(this).val().length == 8) {
            $(this).val($(this).val() + '-');
        }
    });
}

function mascaraTelefoneCelular(celular) {
    $('#' + celular).on('keypress', function () {
        if ($(this).val().length == 0) {
            $(this).val('(' + $(this).val());
        }

        if ($(this).val().length == 3) {
            $(this).val($(this).val() + ')');
        }

        if ($(this).val().length == 9) {
            $(this).val($(this).val() + '-');
        }
    });
}

function formatarMascara(entrada, mascara) {
    var saida = mascara.substring(0, 1);
    var texto = mascara.substring(entrada.value.length);

    if (texto.substring(0, 1) != saida) {
        entrada.value += texto.substring(0, 1);
    }
}

function exibirModal(titulo, mensagem) {
    $('#tituloModal').html(titulo);
    $('#msgModal').html(mensagem);

    $('#modalMensagem').css('z-index', '999999');
    $('#modalMensagem').modal('show');
}

function validarObrigatorios(funcao) {
    var salvar = true;

    $('[validar]').each(function () {
        if (($(this).is(':radio') && !$('input[name=' + $(this).attr('name') + ']:checked').val()) || $(this).val() === null || $(this).val().trim() === '') {
            salvar = mensagemValidacao($(this));
            return false;
        }
    });

    if (salvar) {
        funcao();
    }
}

function mensagemValidacao(campo) {
    exibirModal('Alerta', 'Campo ' + $(campo).attr('validar') + ' é obrigatório.');

    $('form').on('submit', function (e) {
        e.preventDefault();
    });

    return false;
}

function preencherEnderecoPorCep(conteudo) {
    if (!('erro' in conteudo)) {
        $('#selectUf').val(conteudo.uf);
        $('#inpBairro').val(conteudo.bairro);
        $('#inpEndereco').val(conteudo.logradouro);
    }
    else {
        limparCep();
    }
}

function limparCep(mensagem) {
    $('#selectUf').val('');
    $('#inpBairro').val('');
    $('#inpEndereco').val('');

    if (mensagem != null && mensagem != undefined && mensagem.trim() != '') {
        exibirModal('Alerta', mensagem);
    }
}

function buscarCep(valor) {
    var cep = valor.replace(/\D/g, '');

    if (cep.trim() != '') {

        var validacep = /^[0-9]{8}$/;

        if (validacep.test(cep)) {
            $('#selectUf').val('...');
            $('#inpBairro').val('...');
            $('#inpEndereco').val('...');

            var script = document.createElement('script');

            script.src = '//viacep.com.br/ws/' + cep + '/json/?callback=preencherEnderecoPorCep';

            document.body.appendChild(script);

        }
        else {
            limparCep('Formato de CEP inválido.');
        }
    }
    else {
        limparCep();
    }
}

function buscarPacientesFilaEspera() {
    $.ajax({
        url: 'BuscarPacientesFilaEspera',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            var linha = '';

            $.each(data, function (index, value) {
                var ultimaLinha = $('#tblFilaEspera').find('tbody').find('tr:last').attr('id');
                var numeroLinha = 0;

                if (ultimaLinha != numeroLinha && ultimaLinha != undefined) {
                    numeroLinha = parseInt(ultimaLinha.replace('linha', '')) + 1;
                }

                linha = '<tr id="linha' + numeroLinha + '"><td>' + value.CPF + '</td><td>' + value.Nome + '</td><td>' + formatarDataJS(value.DataFilaEspera) + '<td class="actions"><a class="btn btn-info btn-xs" data-toggle="modal" data-target="#ModalAgendar" onclick="indiceAtual = ' + numeroLinha + '">Agendar</a><a style="margin-left: 6px !important;" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-modal" onclick="removerFila(' + numeroLinha  + ')">Remover da fila</a></td></tr>';
                $('#tblFilaEspera').find('tbody').append(linha);

                numeroLinha++;
            });
        }
    });
}

function agendarConsulta(index) {
    $.ajax({
        url: 'AgendarConsulta',
        type: 'get',
        data: {
            CPF: $('#tblFilaEspera').find('#linha' + index).find('td').eq(0).text(),
            aDataAgendamento: formatarDataHoraJS($('#txtDataAgendamento').val())
        },
        success: function () {
            $('#tblFilaEspera').find('#linha' + index).remove();
            $('#txtDataAgendamento').val('');
            exibirModal('Informação', 'Agendamento cadastrado com sucesso. Foi enviado um email para o paciente informando a data da consulta.');
        }
    });
}

function removerFila(index) {
    $.ajax({
        url: 'RemoverFilaEspera',
        type: 'get',
        data: { CPF: $('#tblFilaEspera').find('#linha' + index).find('td').eq(0).text() },
        success: function () {
            $('#tblFilaEspera').find('#linha' + index).remove();
            exibirModal('Informação', 'Paciente removido da fila com sucesso.');
        }
    });
}

function formatarDataHoraJS(data) {
    var ano = data.substr(0, 4);
    var mes = data.substr(data.indexOf('-') + 1, 2);
    var dia = data.substr(data.lastIndexOf('-') + 1, 2);
    var hora = data.substr(data.indexOf('T') + 1, 2);
    var minutos = data.substr(data.indexOf(':') + 1, 2);

    var dataFormatada = dia + '/' + mes + '/' + ano + ' ' + hora + ':' + minutos;

    return dataFormatada;
}

function formatarDataCSharp(data) {
    var mes =  data.substr(0, 2);
    var dia = data.substr(data.indexOf('/') + 1, 2);
    var ano = data.substr(data.lastIndexOf('/') + 1, 4);

    var dataFormatada = dia + '/' + mes + '/' + ano;

    return dataFormatada;
}

function formatarDataHoraJSON(data) {
    var dataFormatada = new Date(parseInt(data.replace("/Date(", "").replace(")/", "")));

    var day = ("0" + dataFormatada.getDate()).slice(-2);
    var month = ("0" + (dataFormatada.getMonth() + 1)).slice(-2);

    var today = (day) + '/' + (month) + '/' + dataFormatada.getFullYear();

    var minutes = dataFormatada.getMinutes();

    if (minutes.toString().length == 1) {
        minutes += '0';
    }

    today = today + ' ' + dataFormatada.getHours() + ':' + minutes;

    return today;
}

function formatarDataJS(data) {
    var dataFormatada = new Date(parseInt(data.replace("/Date(", "").replace(")/", "")));

    var day = ("0" + dataFormatada.getDate()).slice(-2);
    var month = ("0" + (dataFormatada.getMonth() + 1)).slice(-2);

    var today = (day) + '/' + (month) + '/' + dataFormatada.getFullYear();

    return today;
}

function deslogar(){
    $.ajax({
        url: '../Inicio/Deslogar',
        type: 'GET',
        success: function () {
            window.location.href = '../Login/Index'
        }
    });
}