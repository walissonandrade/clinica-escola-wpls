﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace ClinicaEscolaWPLS
{
    public static class WebUtilitario
    {
        public static string RemoverMascara(this string aValor)
        {
            return aValor.Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Replace(".", string.Empty).Replace("/", string.Empty).Replace(@"\", string.Empty);
        }

        public static void EnviarEmail(string aRemetente, string aAssunto, string aMensagem, List<Attachment> aAnexos)
        {
            MailMessage email = new MailMessage();

            email.From = new MailAddress("clinicaescolawpls@gmail.com");
            email.To.Add(aRemetente);
            email.Subject = aAssunto;
            email.Body = aMensagem;
            
            if (aAnexos?.Count > 0)
                aAnexos.ForEach(a => email.Attachments.Add(a));

            using (var smtp = new SmtpClient("smtp.gmail.com"))
            {
                smtp.EnableSsl = true;
                smtp.Port = 587;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;

                smtp.Credentials = new NetworkCredential("clinicaescolawpls@gmail.com", "wpls123456");

                smtp.Send(email);
            }
        }
    }
}