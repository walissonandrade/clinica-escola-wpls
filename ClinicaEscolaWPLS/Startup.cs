﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClinicaEscolaWPLS.Startup))]
namespace ClinicaEscolaWPLS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
