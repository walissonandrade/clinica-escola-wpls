﻿using Enumerador;
using Model;
using Negocio;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitario;

namespace ClinicaEscolaWPLS.Controllers
{
    public class FilaEsperaController : Controller
    {
        // GET: FilaEspera
        public ActionResult Index()
        {
            return View();
        }

        private static PacienteNegocio _PacienteNegocioSingleton;

        static PacienteNegocio _PacienteNegocio
        {
            get
            {
                if (_PacienteNegocioSingleton == null)
                    _PacienteNegocioSingleton = new PacienteNegocio();

                return _PacienteNegocioSingleton;
            }
        }

        [HttpGet]
        public JsonResult BuscarPacientesFilaEspera()
        {
            var paciente = _PacienteNegocio.BuscarPorFiltro(a => a.FilaEspera).ToList().OrderByDescending(o => o.DataFilaEspera);

            return Json(paciente.Select(s => new
            {
                CPF = s.CPF,
                DataFilaEspera = s.DataFilaEspera,
                Nome = s.Nome
            }), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public void RemoverFilaEspera(string CPF)
        {
            CPF = CPF.RemoverMascara();
            MetodoRemoverPacienteDaFila(CPF);
        }

        [HttpGet]
        public void AgendarConsulta(string CPF, string aDataAgendamento)
        {
            DateTime dataAgendamento = aDataAgendamento.FormatarDataHoraString();
            var paciente = MetodoRemoverPacienteDaFila(CPF);

            if (paciente != null)
            {
                AgendamentoEntidade agendamento = new AgendamentoNegocio().CriarAgendamentoPaciente(paciente.PessoaID, dataAgendamento);

                WebUtilitario.EnviarEmail(paciente.Email, "Agendamento de consulta Clínica Escola - Faculdade Pitágoras Betim", 
                                          string.Format(WebUtilitario.MensagemEmail, paciente.Nome, agendamento.DataAgendamento.ToShortDateString(), 
                                                        $"{agendamento.DataAgendamento.Hour}:{agendamento.DataAgendamento.Minute}"), null);
            }
        }

        private PacienteEntidade MetodoRemoverPacienteDaFila(string CPF)
        {
            var pacienteNegocio = new PacienteNegocio();
            var paciente = pacienteNegocio.BuscarPorFiltro(a => a.CPF == CPF).FirstOrDefault();

            if (paciente != null)
            {
                paciente.FilaEspera = false;
                pacienteNegocio.Atualizar(paciente);
                pacienteNegocio.Salvar();
            }

            return paciente;
        }

        [HttpGet]
        public string InserirClienteFila(string aValor, string aCpfOuNome)
        {
            var paciente = _PacienteNegocio.BuscarPacientePorNomeOuCPF(aValor, aCpfOuNome);

            paciente.FilaEspera = true;
            paciente.DataFilaEspera = DateTime.Now;

            _PacienteNegocio.Atualizar(paciente);
            _PacienteNegocio.Salvar();

            return paciente.DataFilaEspera.ToString("MM/dd/yyyy HH:mm");
        }
    }
}