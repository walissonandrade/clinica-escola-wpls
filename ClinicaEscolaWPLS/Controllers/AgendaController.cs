﻿using Model;
using Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Utilitario;

namespace ClinicaEscolaWPLS.Controllers
{
    public class AgendaController : Controller
    {
        // GET: Agenda
        public ActionResult Index()
        {
            return View();
        }

        private static PacienteNegocio _PacienteNegocioSingleton;

        static PacienteNegocio _PacienteNegocio
        {
            get
            {
                if (_PacienteNegocioSingleton == null)
                    _PacienteNegocioSingleton = new PacienteNegocio();

                return _PacienteNegocioSingleton;
            }
        }

        private static AgendamentoNegocio _AgendamentoNegocioSingleton;

        static AgendamentoNegocio _AgendamentoNegocio
        {
            get
            {
                if (_AgendamentoNegocioSingleton == null)
                    _AgendamentoNegocioSingleton = new AgendamentoNegocio();

                return _AgendamentoNegocioSingleton;
            }
        }

        [HttpGet]
        public JsonResult BuscarListaAgendamentos()
        {
            var agendamentos = _AgendamentoNegocio.Buscar().ToList().OrderBy(o => o.DataAgendamento).Select(s => new
            {
                Codigo = s.AgendamentoID,
                Nome = s.Paciente.Nome,
                DataAgendamento = s.DataAgendamento
            });

            return Json(agendamentos, JsonRequestBehavior.AllowGet);
        }

        public void RemoverAgendamento(long Codigo)
        {
            var agendamento = _AgendamentoNegocio.BuscarPorIdentificador(Codigo);

            _AgendamentoNegocio.Deletar(agendamento);
            _AgendamentoNegocio.Salvar();
        }

        [HttpGet]
        public JsonResult CriarAgendamentoPaciente(string aValor, string aCpfOuNome, string aDataAgendamento)
        {
            var paciente = _PacienteNegocio.BuscarPacientePorNomeOuCPF(aValor, aCpfOuNome);

            if (paciente != null)
            {
                DateTime dataAgendamento = aDataAgendamento.FormatarDataHoraString();
                AgendamentoEntidade agendamento = new AgendamentoNegocio().CriarAgendamentoPaciente(paciente.PessoaID, dataAgendamento);

                WebUtilitario.EnviarEmail(paciente.Email, "Agendamento de consulta Clínica Escola - Faculdade Pitágoras Betim",
                                          string.Format(WebUtilitario.MensagemEmail, paciente.Nome, agendamento.DataAgendamento.ToShortDateString(),
                                                        $"{agendamento.DataAgendamento.Hour}:{agendamento.DataAgendamento.Minute}"), null);

                return Json(agendamento, JsonRequestBehavior.AllowGet);
            }

            return Json("Paciente não encontrado.", JsonRequestBehavior.AllowGet);
        }
    }
}