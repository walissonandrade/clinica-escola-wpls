﻿using Enumerador;
using Model;
using Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using System.Xml;
using Utilitario;

namespace ClinicaEscolaWPLS.Controllers
{
    public class CadastroClienteController : Controller
    {
        // GET: CadastroCliente
        public ActionResult Index()
        {
            return View();
        }

        private static PacienteNegocio _PacienteNegocioSingleton;

        static PacienteNegocio _PacienteNegocio
        {
            get
            {
                if (_PacienteNegocioSingleton == null)
                    _PacienteNegocioSingleton = new PacienteNegocio();

                return _PacienteNegocioSingleton;
            }
        }

        [HttpPost]
        public void CadastrarCliente(string Nome, string TelefoneFixo, string TelefoneCelular, string Email, string CPF, string RG, string Sexo,
                                     string EstadoCivil, short? QuantidadeFilhos, string Escolaridade, string Profissao, string DescricaoConhecimentoClinica,
                                     string InstituicaoEncaminhamento, string CursoFAPBetim, string Endereco, string Bairro, string CEP, string Estado,
                                     DateTime DataNascimento)
        {
            PacienteEntidade paciente = new PacienteEntidade();

            paciente.Pais = "Brasil";
            paciente.Estado = Estado;
            paciente.Cep = CEP.RemoverMascara();
            paciente.Bairro = Bairro;
            paciente.Logradouro = Endereco;
            paciente.DataFilaEspera = DateTime.Now;
            paciente.Nome = Nome;
            paciente.TelefoneFixo = TelefoneFixo.RemoverMascara();
            paciente.TelefoneCelular = TelefoneCelular.RemoverMascara();
            paciente.Email = Email;
            paciente.CPF = CPF.RemoverMascara();
            paciente.RG = RG.RemoverMascara();
            paciente.DataNascimento = DataNascimento;

            if (!string.IsNullOrWhiteSpace(Sexo))
                paciente.Sexo = Sexo.ComoEnum<SexoEnum>().Value;
            else
                paciente.Sexo = SexoEnum.NaoDeclarado;

            paciente.EstadoCivil = EstadoCivilEnum.Solteiro;//EstadoCivil.ComoEnum<EstadoCivilEnum>().Value;
            paciente.QuantidadeFilhos = (byte)QuantidadeFilhos.GetValueOrDefault(0);
            paciente.Escolaridade = EscolaridadeEnum.EnsinoSuperiorCompleto;//Escolaridade.ComoEnum<EscolaridadeEnum>().Value;
            paciente.Profissao = Profissao;
            paciente.DescricaoConhecimentoClinica = DescricaoConhecimentoClinica;
            paciente.InstituicaoEncaminhamento = InstituicaoEncaminhamento;
            paciente.CursoFAPBetim = CursoFAPBetim;
            paciente.FilaEspera = true;

            _PacienteNegocio.Inserir(paciente);
            _PacienteNegocio.Salvar();

            //return JavaScript("exibirModal('Informação', 'Dados atualizados com sucesso.')");
        }

        [HttpGet]
        public JsonResult PreencherDropDownCidade(int aIDEstado)
        {
            string xml;
            string tag;

            xml = Server.MapPath(@"\XML\cidades.xml");
            tag = "CIDADE";

            var itens = new List<SelectListItem>();
            XmlDocument documento = new XmlDocument();
            documento.Load(xml);

            for (int i = 0; i < documento.GetElementsByTagName(tag).Count; i++)
            {
                XmlNode node = documento.GetElementsByTagName(tag)[i];

                if (Convert.ToInt32(node.SelectSingleNode("IDESTADO").InnerText) == aIDEstado)
                {
                    itens.Add(new SelectListItem
                    {
                        Value = node.SelectSingleNode("ID").InnerText,
                        Text = node.SelectSingleNode("NOME").InnerText
                    });
                }
            }

            itens.Insert(0, new SelectListItem());
            itens[0].Text = string.Empty;

            return Json(itens, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult PreencherDropDown(string aXML)
        {
            string xml;
            string tag;

            switch (aXML)
            {
                case "Estados":
                    xml = Server.MapPath(@"\XML\estados.xml");
                    tag = "ESTADO";
                    break;

                case "Cidades":
                    xml = Server.MapPath(@"\XML\cidades.xml");
                    tag = "CIDADE";
                    break;

                default:
                    xml = string.Empty;
                    tag = string.Empty;
                    break;
            }

            var itens = new List<SelectListItem>();
            XmlDocument documento = new XmlDocument();
            documento.Load(xml);

            for (int i = 0; i < documento.GetElementsByTagName(tag).Count; i++)
            {
                XmlNode node = documento.GetElementsByTagName(tag)[i];

                itens.Add(new SelectListItem
                {
                    Value = node.SelectSingleNode("ID").InnerText,
                    Text = node.SelectSingleNode("NOME").InnerText
                });
            }

            itens.Insert(0, new SelectListItem());
            itens[0].Text = string.Empty;

            return Json(itens, JsonRequestBehavior.AllowGet);
        }

        public List<string> AutoCompleteBuscaCliente(string aValor, string aCpfOuNome)
        {
            var listaPacientes = new List<string>();

            if (!string.IsNullOrWhiteSpace(aCpfOuNome))
            {
                if (aCpfOuNome == "Nome")
                {
                    listaPacientes = _PacienteNegocio.BuscarPorFiltro(a => a.Nome.Contains(aValor)).Select(s => s.Nome).ToList();
                }
                else if (aCpfOuNome == "CPF")
                {
                    listaPacientes = _PacienteNegocio.BuscarPorFiltro(a => a.CPF.Contains(aValor)).Select(s => s.Nome).ToList();
                }
            }

            return listaPacientes;
        }

        [HttpGet]
        public JsonResult BuscarCliente(string aValor, string aCpfOuNome)
        {
            PacienteEntidade pacienteEntidade = _PacienteNegocio.BuscarPacientePorNomeOuCPF(aValor, aCpfOuNome);

            if (pacienteEntidade != null)
                return Json(pacienteEntidade, JsonRequestBehavior.AllowGet);
            else
                return Json("Paciente não encontrado.", JsonRequestBehavior.AllowGet);
        }

        public JsonResult AlterarPaciente(string Nome, string TelefoneFixo, string TelefoneCelular, string Email, string CPF, string Sexo,
                                          string Endereco, string Bairro, string CEP, string Estado, DateTime DataNascimento)
        {
            PacienteEntidade paciente = _PacienteNegocio.BuscarPacientePorNomeOuCPF(CPF.RemoverMascara(), "CPF");

            if (paciente != null)
            {
                paciente.Estado = Estado;
                paciente.Cep = CEP.RemoverMascara();
                paciente.Bairro = Bairro;
                paciente.Logradouro = Endereco;
                paciente.Nome = Nome;
                paciente.TelefoneFixo = TelefoneFixo.RemoverMascara();
                paciente.TelefoneCelular = TelefoneCelular.RemoverMascara();
                paciente.Email = Email;
                paciente.CPF = CPF.RemoverMascara();
                paciente.DataNascimento = DataNascimento;

                if (!string.IsNullOrWhiteSpace(Sexo))
                    paciente.Sexo = Sexo.ComoEnum<SexoEnum>().Value;
                else
                    paciente.Sexo = SexoEnum.NaoDeclarado;

                paciente.EstadoCivil = EstadoCivilEnum.Solteiro;
                paciente.QuantidadeFilhos = 0;
                paciente.Escolaridade = EscolaridadeEnum.EnsinoSuperiorCompleto;
                paciente.Profissao = string.Empty;
                paciente.DescricaoConhecimentoClinica = string.Empty;
                paciente.InstituicaoEncaminhamento = string.Empty;
                paciente.CursoFAPBetim = string.Empty;

                _PacienteNegocio.Atualizar(paciente);
                _PacienteNegocio.Salvar();

                return Json(paciente, JsonRequestBehavior.AllowGet);
            }
            else
                return Json("Paciente não encontrado.", JsonRequestBehavior.AllowGet);
        }
    }
}