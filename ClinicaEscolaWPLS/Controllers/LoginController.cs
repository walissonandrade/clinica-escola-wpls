﻿using Negocio;
using System.Linq;
using System.Web.Mvc;

namespace ClinicaEscolaWPLS.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ValidarLogin(FormCollection aForm)
        {
            string login = aForm["inputUsuario"].ToString();
            string senha = aForm["inputPassword"].ToString();

            if (!string.IsNullOrWhiteSpace(login) && !string.IsNullOrWhiteSpace(senha))
            {
                bool usuarioCadastrado = new LoginNegocio().BuscarPorFiltro(a => a.Login.ToUpper() == login.ToUpper() && a.Senha == senha).Any();

                if (usuarioCadastrado)
                {
                    Session["UsuarioLogado"] = login;
                    return View("~/Views/Inicio/Index.cshtml");
                }
            }

            return View();
        }
    }
}