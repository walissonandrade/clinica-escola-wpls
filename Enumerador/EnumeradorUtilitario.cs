﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enumerador
{
    public static class EnumeradorUtilitario
    {
        public static T? ComoEnum<T>(this string aValor) where T : struct
        {
            if (!string.IsNullOrWhiteSpace(aValor))
            {
                T enumerador;

                Enum.TryParse<T>(aValor, out enumerador);

                return enumerador;
            }

            return null;
        }
    }
}