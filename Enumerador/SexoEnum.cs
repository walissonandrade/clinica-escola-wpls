﻿using System.ComponentModel;

namespace Enumerador
{
    public enum SexoEnum
    {
        [Description("Masculino")]
        Masculino = 1,

        [Description("Feminino")]
        Feminino = 2,

        [Description("Não declarado")]
        NaoDeclarado = 3
    }
}