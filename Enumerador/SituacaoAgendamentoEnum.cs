﻿using System.ComponentModel;

namespace Enumerador
{
    public enum SituacaoAgendamentoEnum
    {
        [Description("Não atendido")]
        NaoAtendido = 1,

        [Description("Atendido")]
        Atendido = 2
    }
}