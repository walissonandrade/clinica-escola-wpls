﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enumerador
{
    public enum EscolaridadeEnum
    {
        [Description("Ensino Fundamental Incompleto")]
        EnsinoFundamentalIncompleto = 1,

        [Description("Ensino Fundamental Completo")]
        EnsinoFundamentalCompleto = 2,

        [Description("Ensino Médio Incompleto")]
        EnsinoMedioIncompleto = 3,

        [Description("Ensino Médio Completo")]
        EnsinoMedioCompleto = 4,

        [Description("Ensino Superior Incompleto")]
        EnsinoSuperiorIncompleto = 5,

        [Description("Ensino Superior Completo")]
        EnsinoSuperiorCompleto = 6,

        [Description("Pós Graduação")]
        PosGraduacao = 7,

        [Description("PHD")]
        PHD = 8
    }
}