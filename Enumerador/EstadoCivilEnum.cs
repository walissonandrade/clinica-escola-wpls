﻿using System.ComponentModel;

namespace Enumerador
{
    public enum EstadoCivilEnum
    {
        [Description("Solteiro")]
        Solteiro = 1,

        [Description("Casado")]
        Casado = 2,

        [Description("Divorciado")]
        Divorciado = 3,

        [Description("Viúvo")]
        Viuvo = 4,

        [Description("União Estável")]
        UniaoEstavel = 5
    }
}