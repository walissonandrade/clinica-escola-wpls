﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

namespace Utilitario
{
    public static class WebUtilitario
    {
        public const string MensagemEmail = "Prezado aluno(a) {0},\nSua consulta foi agendada para o dia {1} às {2} horas.";

        public static string RemoverMascara(this string aValor)
        {
            return aValor.Replace("-", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty).Replace(".", string.Empty).Replace("/", string.Empty).Replace(@"\", string.Empty);
        }

        public static string AdicionarMascaraCPF(this string aCPF)
        {
            return aCPF.Insert(3, ".").Insert(7, ".").Insert(11, "-");
        }

        public static DateTime FormatarDataHoraString(this string aData)
        {
            int dia = Convert.ToInt32(aData.Substring(0, 2));
            int mes = Convert.ToInt32(aData.Substring(3, 2));
            int ano = Convert.ToInt32(aData.Substring(6, 4));
            int hora = Convert.ToInt32(aData.Substring(11, 2));
            int minutos = Convert.ToInt32(aData.Substring(14, 2));

            return new DateTime(ano, mes, dia, hora, minutos, 0);
        }

        public static void EnviarEmail(string aRemetente, string aAssunto, string aMensagem, List<Attachment> aAnexos)
        {
            MailMessage email = new MailMessage();

            email.From = new MailAddress("clinicaescolawpls@gmail.com");
            email.To.Add(aRemetente);
            email.Subject = aAssunto;
            email.Body = aMensagem;

            if (aAnexos?.Count > 0)
                aAnexos.ForEach(a => email.Attachments.Add(a));

            using (var smtp = new SmtpClient("smtp.gmail.com"))
            {
                smtp.EnableSsl = true;
                smtp.Port = 587;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;

                smtp.Credentials = new NetworkCredential("clinicaescolawpls@gmail.com", "wpls123456");

                smtp.Send(email);
            }
        }
    }
}