﻿using System;
using System.Collections.Generic;

namespace Model
{
    public class PacienteEntidade : PessoaEntidade
    {
        public PacienteEntidade()
        {
            Agendamentos = new List<AgendamentoEntidade>();
        }

        public DateTime DataFilaEspera { get; set; }
        public bool FilaEspera { get; set; }

        public virtual ICollection<AgendamentoEntidade> Agendamentos { get; set; }
    }
}