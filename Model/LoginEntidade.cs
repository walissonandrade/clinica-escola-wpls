﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    [Table("Login")]
    public class LoginEntidade : EntidadeBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long LoginID { get; set; }
        [Required, Index(IsUnique = true), MaxLength(30)]
        public string Login { get; set; }
        [Required, MaxLength(30)]
        public string Senha { get; set; }
        [Required, MaxLength(70)]
        public string RespostaSecreta { get; set; }
        [MaxLength(80)]
        public string Email { get; set; }
    }
}