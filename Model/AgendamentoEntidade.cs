﻿using Enumerador;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    [Table("Agendamento")]
    public class AgendamentoEntidade : EntidadeBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long AgendamentoID { get; set; }
        public DateTime DataAgendamento { get; set; }
        public SituacaoAgendamentoEnum SituacaoAgendamento { get; set; }

        public long PacienteID { get; set; }
        [ForeignKey("PacienteID")]
        public virtual PacienteEntidade Paciente { get; set; }
    }
}