﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class EnderecoEntidade : EntidadeBase
    {
        [Key, ForeignKey("Pessoa")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long EnderecoID { get; set; }

        public virtual PessoaEntidade Pessoa { get; set; }
    }
}