﻿using Enumerador;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    [Table("Pessoa")]
    public abstract class PessoaEntidade : EntidadeBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long PessoaID { get; set; }
        [MaxLength(90), Required]
        public string Nome { get; set; }
        [MaxLength(14)]
        public string TelefoneFixo { get; set; }
        [MaxLength(14)]
        public string TelefoneCelular { get; set; }
        [MaxLength(90)]
        public string Email { get; set; }
        public string CPF { get; set; }
        public string RG { get; set; }
        public DateTime DataNascimento { get; set; }
        public SexoEnum Sexo { get; set; }
        public EstadoCivilEnum EstadoCivil { get; set; }
        public byte QuantidadeFilhos { get; set; }
        public EscolaridadeEnum Escolaridade { get; set; }
        public string Profissao { get; set; }
        public string DescricaoConhecimentoClinica { get; set; }
        public string InstituicaoEncaminhamento { get; set; }
        public string CursoFAPBetim { get; set; }
        [MaxLength(90), Required]
        public string Logradouro { get; set; }
        public int? Numero { get; set; }
        public string Cep { get; set; }
        [MaxLength(80)]
        public string Bairro { get; set; }
        [MaxLength(80)]
        public string Cidade { get; set; }
        [MaxLength(40), Required]
        public string Estado { get; set; }
        [MaxLength(40), Required]
        public string Pais { get; set; }
    }
}